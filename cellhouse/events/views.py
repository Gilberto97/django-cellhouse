from django.http import HttpResponse
from datetime import datetime, time
from swingtime import models as swingtime
from dateutil import rrule
import pytz

def index(request):

    event = swingtime.create_event(
        'Beginner Class',
        ('bgn', 'Beginner Classes'),
        description='Open to all beginners',
        start_time=datetime.combine(datetime.now().date(), time(19), tzinfo=pytz.UTC),
        count=6,
        byweekday=(rrule.MO, rrule.WE, rrule.FR)
    )

    for o in event.occurrence_set.all():
        print (o)
    return HttpResponse("Hello, world. You're at the polls index.")