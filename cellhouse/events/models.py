# from typing import Counter
from django.db import models
from users.models import User
from households.models import Household
from swingtime.models import EventType, Event as EventBaseModel, Occurrence as OccurrenceBaseModel
from django.utils.translation import gettext_lazy as _

class Event(EventBaseModel):
    added_by = models.ForeignKey(
        User,
        verbose_name=_('added by'),
        on_delete=models.DO_NOTHING
    )
    household = models.ForeignKey(
        Household,
        verbose_name=_('household'),
        on_delete=models.DO_NOTHING
    )

    def add_occurrences(self, assigned_to, start_time, end_time, **rrule_params):
        '''
        Add one or more occurences to the event using a comparable API to
        ``dateutil.rrule``.

        If ``rrule_params`` does not contain a ``freq``, one will be defaulted
        to ``rrule.DAILY``.

        Because ``rrule.rrule`` returns an iterator that can essentially be
        unbounded, we need to slightly alter the expected behavior here in order
        to enforce a finite number of occurrence creation.

        If both ``count`` and ``until`` entries are missing from ``rrule_params``,
        only a single ``Occurrence`` instance will be created using the exact
        ``start_time`` and ``end_time`` values.
        '''
        count = rrule_params.get('count')
        until = rrule_params.get('until')
        if not (count or until):
            self.occurrence_set.create(assigned_to=assigned_to, start_time=start_time, end_time=end_time)
        else:
            rrule_params.setdefault('freq', rrule.DAILY)
            delta = end_time - start_time
            occurrences = []
            for ev in rrule.rrule(dtstart=start_time, **rrule_params):
                occurrences.append(Occurrence(assigned_to=assigned_to, start_time=ev, end_time=ev + delta, event=self))
            self.occurrence_set.bulk_create(occurrences)


class Occurrence(OccurrenceBaseModel):
    assigned_to = models.ForeignKey(
        User,
        verbose_name=_('assigned to'),
        on_delete=models.DO_NOTHING
    )
   

def create_event(
    title,
    event_type,
    household,
    added_by,
    assigned_to,
    description='',
    start_time=None,
    end_time=None,
    note=None,
    **rrule_params
):
    '''
    Convenience function to create an ``Event``, optionally create an
    ``EventType``, and associated ``Occurrence``s. ``Occurrence`` creation
    rules match those for ``Event.add_occurrences``.

    Returns the newly created ``Event`` instance.

    Parameters

    ``event_type``
        can be either an ``EventType`` object or 2-tuple of ``(abbreviation,label)``,
        from which an ``EventType`` is either created or retrieved.

    ``start_time``
        will default to the current hour if ``None``

    ``end_time``
        will default to ``start_time`` plus swingtime_settings.DEFAULT_OCCURRENCE_DURATION
        hour if ``None``

    ``freq``, ``count``, ``rrule_params``
        follow the ``dateutils`` API (see http://labix.org/python-dateutil)

    '''

    if isinstance(event_type, tuple):
        event_type, created = EventType.objects.get_or_create(
            abbr=event_type[0],
            label=event_type[1]
        )

    event = Event.objects.create(
        title=title,
        description=description,
        event_type=event_type,
        household=household,
        added_by=added_by
    )

    if note is not None:
        event.notes.create(note=note)

    start_time = start_time or datetime.now().replace(
        minute=0,
        second=0,
        microsecond=0
    )

    end_time = end_time or (start_time + swingtime_settings.DEFAULT_OCCURRENCE_DURATION)
    event.add_occurrences(assigned_to, start_time, end_time, **rrule_params)
    return event


