from django.test import TestCase
from datetime import datetime
from django.utils import timezone
import pytz

from users.models import User
from households.models import Household, Membership
from .models import Event, Occurrence

class TripTestCase(TestCase):
    def setUp(self):

        householder = User.objects.create_user(
            email='przemekbaj1@gmail.com', password='secret123',
            first_name="Przemyslaw", last_name="Baj"
        )

        household = Household.objects.create(name="My House", address="121 Gadebridge Road, Hemel Hempstead", created_by=householder)

        roommate1 = User.objects.create_user(
            email='kacpersmyk@gmail.com', password='secret123',
            first_name="Kacper", last_name="Smyk"
        )
        roommate2 = User.objects.create_user(
            email='sebawojcik@gmail.com', password='secret123',
            first_name="Sebastian", last_name="Wojcik"
        )

        Membership.objects.create(
          user=roommate1,
          household=household,
          invited_by=householder
        )
        Membership.objects.create(
          user=roommate2,
          household=household,
          invited_by=householder
        )

    def test_household_created(self):
        household = Household.objects.get(name="My House")
        is_household_created = bool(household)
        self.assertEqual(is_household_created,True)

    def test_memberships_created(self):
        household = Household.objects.get(name="My House")
        memberships_len = household.memberships.count()
        self.assertEqual(memberships_len,2)
