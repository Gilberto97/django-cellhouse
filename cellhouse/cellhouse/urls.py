from django.contrib import admin
from django.urls import path, include
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

urlpatterns = [
    path('admin/', admin.site.urls),
    # path('api/', include('users.urls', 'user_api')),
    # path('api/', include('events.urls', 'events_api')),
    # path('api/', include('households.urls', 'households_api')),
    path('api/households/', include('households.urls', 'household_api')),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]
