from django.urls import path
from .views import api_detail_household_view

app_name = 'households'

urlpatterns = [
    path('<slug>', api_detail_household_view, name='detail'),
]
