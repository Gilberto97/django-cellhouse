from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view

from users.models import User
from .models import Household, Membership
from .serializers import HouseholdSerializer


@api_view(['POST', ])
def api_create_household_view(request):

    user = User.objects.get(pk=1)

    household = Household(created_by=user)

    if request.method == 'POST':
        serializer = HouseholdSerializer(household, data=request.data)
        if(serializer.is_valid()):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)
    # try:
    #     household = Household.objects.get(slug=slug)
    # except Household.DoesNotExist:
    #     return Response(status=status.HTTP_404_NOT_FOUND)
    
    # if request.method == "GET":
    #    serializer = HouseholdSerializer(household)
    #    return Response(serializer.data)

@api_view(['GET', ])
def api_detail_household_view(request, slug):
    try:
        household = Household.objects.get(slug=slug)
    except Household.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    
    if request.method == "GET":
       serializer = HouseholdSerializer(household)
       return Response(serializer.data)

@api_view(['PATCH', ])
def api_update_household_view(request, slug):
    try:
        household = Household.objects.get(slug=slug)
    except Household.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    
    if request.method == "PUT":
        serializer = HouseholdSerializer(household, data=request.data)
        data = {}
        if serializer.is_valid():
            serializer.save()
            data["success"] = "updated successful"
            return Response(serializer.data)    

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['DELETE', ])
def api_delete_household_view(request, slug):
    try:
        household = Household.objects.get(slug=slug)
    except Household.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    
    if request.method == "DELETE":
        operation = household.delete()
        data = {}
        if operation:
            data["success"] = "deleted successful"
        else:
            data["failure"] = "delete failed"

        return Response(data=data)


# Create your views here.
