from rest_framework import serializers

from .models import Household

class HouseholdSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Household
        fields = ('name', 'address', 'created_by')