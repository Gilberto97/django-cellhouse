from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.conf import settings as django_settings

class Household(models.Model):
    name = models.CharField(_('title'),max_length=50)
    address = models.CharField(_('address'), max_length=200)
    created_by = models.ForeignKey(
        django_settings.AUTH_USER_MODEL,
        verbose_name=_('created by'),
        related_name="households_created",
        on_delete=models.DO_NOTHING
    )
    memberships = models.ManyToManyField(
        django_settings.AUTH_USER_MODEL, 
        through='Membership',
        through_fields=('household','user')
    )


class Membership(models.Model):
    user = models.ForeignKey(
        django_settings.AUTH_USER_MODEL, 
        on_delete=models.CASCADE,
    )
    household = models.ForeignKey(
        Household, 
        on_delete=models.CASCADE,
    )
    accepted = models.BooleanField(default=False)
    date_joined = models.DateTimeField(
        _('date joined'), 
        null=True, 
        default=None
    )
    invited_by = models.ForeignKey(
        django_settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name=_("invited by"),
        related_name="invites",
    )


# Create your models here.
